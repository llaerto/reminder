package com.llaerto.remindernew.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.llaerto.remindernew.MainActivity;
import com.llaerto.remindernew.R;
import com.llaerto.remindernew.adapters.TaskAdapter;
import com.llaerto.remindernew.alarm.AlarmHelper;
import com.llaerto.remindernew.model.Item;
import com.llaerto.remindernew.model.MyTask;

public abstract class TaskFragment extends Fragment {
    protected RecyclerView recyclerView;
    protected RecyclerView.LayoutManager layoutManager;

    protected TaskAdapter adapter;

    public AlarmHelper alarmHelper;
    public MainActivity activity;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            activity = (MainActivity) getActivity();
        }

        alarmHelper = AlarmHelper.getInstance();
        addTaskFromDB();
    }

    public abstract void addTask(MyTask myTask, boolean saveToDb);

    public void updateTask(MyTask myTask) {
        adapter.updateTask(myTask);
    }

    public void removeTaskDialog(final int location) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_dialog);
        Item item = adapter.getItem(location);

        if (item.isTask()) {
            MyTask removingTask = (MyTask) item;
            final long timeStamp = removingTask.getTimeStamp();
            final boolean[] isDeleted = {false};

            builder.setPositiveButton(R.string.delete_dialog_positive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    adapter.removeItem(location);
                    isDeleted[0] = true;
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.coordinator),
                            R.string.removed, Snackbar.LENGTH_LONG);
                    snackbar.setAction(R.string.delete_dialog_negative, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addTask(activity.dBhelper.query().getTask(timeStamp), false);
                            isDeleted[0] = false;
                        }
                    });
                    snackbar.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                        @Override
                        public void onViewAttachedToWindow(View v) {

                        }

                        @Override
                        public void onViewDetachedFromWindow(View v) {
                            if (isDeleted[0]) {
                                alarmHelper.removeAlarm(timeStamp);
                                activity.dBhelper.removeTask(timeStamp);
                            }
                        }
                    });
                    snackbar.show();

                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(R.string.delete_dialog_negative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        builder.show();

    }

    public void showTaskEditFragment(MyTask myTask) {
        Fragment fragment = TaskEditFragment.newInstance(myTask);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.add(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public abstract void checkAdapter();

    public abstract void addTaskFromDB();

    public abstract void moveTask(MyTask myTask);

    public abstract void findTask(String title);
}
