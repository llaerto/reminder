package com.llaerto.remindernew.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.llaerto.remindernew.R;
import com.llaerto.remindernew.Utils;
import com.llaerto.remindernew.alarm.AlarmHelper;
import com.llaerto.remindernew.model.MyTask;

import java.util.Calendar;

public class TaskCreateFragment extends Fragment {
    private MyTask myTask;
    MenuItem saveItem;
    EditText titleEditText;
    EditText timeEditText;
    EditText dateEditText;
    EditText descrEditText;
    CheckBox checkBox;
    LinearLayout linearLayout;
    FragmentManager fragmentManager;
    Calendar calendar;
    AddingTaskListener addingTaskListener;

    public interface AddingTaskListener {
        void onTaskAdded(MyTask myTask);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myTask = new MyTask();
        fragmentManager = getFragmentManager();
        setRetainInstance(true);
        calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 1);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        addingTaskListener = (AddingTaskListener) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_create);
        if (toolbar != null) {
            TextView textToolbar = (TextView) view.findViewById(R.id.toolbar_title_create);
            toolbar.setTitleTextColor(ContextCompat.getColor(getActivity(), R.color.icons));
            textToolbar.setText(R.string.new_task_title);
            ImageButton imageButton = (ImageButton) view.findViewById(R.id.create_back_button);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myTask = null;
                    getFragmentManager().popBackStack();
                }
            });
            linearLayout = (LinearLayout) view.findViewById(R.id.confirm_button);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveTask();
                    AlarmHelper alarmHelper = AlarmHelper.getInstance();
                    alarmHelper.setAlarm(myTask);
                    addingTaskListener.onTaskAdded(myTask);
                    getFragmentManager().popBackStack();
                }
            });

        }

        TextInputLayout tilTitle = (TextInputLayout) view.findViewById(R.id.create_til_title);
        titleEditText = tilTitle.getEditText();

        TextInputLayout tilDate = (TextInputLayout) view.findViewById(R.id.create_til_date);
        dateEditText = tilDate.getEditText();
        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dateEditText.length() == 0) {
                    dateEditText.setText(" ");
                }
                DatePickerFragment datePickerFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        dateEditText.setText(Utils.getDate(calendar.getTimeInMillis()));
                    }

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dateEditText.setText(null);
                    }
                };
                datePickerFragment.show(fragmentManager, "date_dialog");
            }
        });

        TextInputLayout tilTime = (TextInputLayout) view.findViewById(R.id.create_til_time);
        timeEditText = tilTime.getEditText();
        timeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeEditText.length() == 0)
                    timeEditText.setText(" ");
                TimePickerFragment timePickerFragment = new TimePickerFragment() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
                        timeEditText.setText(Utils.getTime(calendar.getTimeInMillis()));
                    }

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        timeEditText.setText(null);
                    }
                };
                timePickerFragment.show(fragmentManager, "time_dialog");
            }
        });

        TextInputLayout tilDescr = (TextInputLayout) view.findViewById(R.id.create_til_descr);
        descrEditText = tilDescr.getEditText();

        checkBox = (CheckBox) view.findViewById(R.id.create_checkbox);
        checkBox.setChecked(false);
        titleEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create, menu);
        saveItem = menu.findItem(R.id.menu_create_save);
        if (titleEditText.length() > 0)
            saveItem.setEnabled(true);
    }

    private void saveTask() {
        myTask.setTitle(titleEditText.getText().toString());
        myTask.setDescr(descrEditText.getText().toString());
        if (dateEditText.length() != 0 || timeEditText.length() != 0) {
            myTask.setDate(calendar.getTimeInMillis());
        }
        myTask.setCritical(checkBox.isChecked());
        myTask.setStatus(MyTask.STATUS_CURRENT);
        Log.d("TAG", "saveTask onCreate " + myTask.getTitle());
    }

}
