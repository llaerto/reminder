package com.llaerto.remindernew.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.llaerto.remindernew.MainActivity;
import com.llaerto.remindernew.R;
import com.llaerto.remindernew.Utils;
import com.llaerto.remindernew.alarm.AlarmHelper;
import com.llaerto.remindernew.model.MyTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class TaskEditFragment extends TaskCreateFragment {

    private MyTask myTask;
    private Calendar calendar;
    private List<MyTask> tasks;
    private MainActivity activity;

    private EditText titleEditText;
    private EditText timeEditText;
    private EditText dateEditText;
    private EditText descrEditText;
    private FragmentManager fragmentManager;
    private CheckBox checkBox;

    public static TaskEditFragment newInstance(MyTask myTask) {

        TaskEditFragment taskEditFragment = new TaskEditFragment();

        Bundle args = new Bundle();
        args.putString("title", myTask.getTitle());
        args.putLong("date", myTask.getDate());
        args.putString("descr", myTask.getDescr());
        args.putBoolean("iscrit", myTask.isCritical());
        args.putLong("timestamp", myTask.getTimeStamp());

        taskEditFragment.setArguments(args);

        return taskEditFragment;
    }

    private TaskEditListener taskEditListener;

    public interface TaskEditListener {
        void onTaskEditListener(MyTask updateTask);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        taskEditListener = (TaskEditListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        String title = args.getString("title");
        long date = args.getLong("date");
        String descr = args.getString("descr");
        boolean isCritical = args.getBoolean("iscrit");
        long timeStamp = args.getLong("timestamp");
        myTask = new MyTask(title, descr, date, isCritical, 0, timeStamp);
        fragmentManager = getFragmentManager();
        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(myTask.getDate());
        activity = (MainActivity) getActivity();
        initTasks();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_create);
        if (toolbar != null) {
            TextView textToolbar = (TextView) view.findViewById(R.id.toolbar_title_create);
            toolbar.setTitleTextColor(ContextCompat.getColor(getActivity(), R.color.icons));
            textToolbar.setText(myTask.getTitle());
            ImageButton imageButton = (ImageButton) view.findViewById(R.id.create_back_button);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFragmentManager().popBackStack();
                }
            });
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.confirm_button);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveTask();
                    AlarmHelper alarmHelper = AlarmHelper.getInstance();
                    alarmHelper.setAlarm(myTask);
                    taskEditListener.onTaskEditListener(myTask);
                    getFragmentManager().popBackStack();
                }
            });

        }

        TextInputLayout tilTitle = (TextInputLayout) view.findViewById(R.id.create_til_title);
        titleEditText = tilTitle.getEditText();

        TextInputLayout tilDate = (TextInputLayout) view.findViewById(R.id.create_til_date);
        dateEditText = tilDate.getEditText();
        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dateEditText.length() == 0) {
                    dateEditText.setText(" ");
                }
                DatePickerFragment datePickerFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year, monthOfYear, dayOfMonth);
                        dateEditText.setText(Utils.getDate(calendar.getTimeInMillis()));
                    }

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dateEditText.setText(null);
                    }
                };
                datePickerFragment.show(fragmentManager, "date_dialog");
            }
        });

        TextInputLayout tilTime = (TextInputLayout) view.findViewById(R.id.create_til_time);
        timeEditText = tilTime.getEditText();
        timeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeEditText.length() == 0)
                    timeEditText.setText(" ");
                TimePickerFragment timePickerFragment = new TimePickerFragment() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
                        timeEditText.setText(Utils.getTime(calendar.getTimeInMillis()));
                    }

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        timeEditText.setText(null);
                    }
                };
                timePickerFragment.show(fragmentManager, "time_dialog");
            }
        });

        TextInputLayout tilDescr = (TextInputLayout) view.findViewById(R.id.create_til_descr);
        descrEditText = tilDescr.getEditText();

        checkBox = (CheckBox) view.findViewById(R.id.create_checkbox);
        checkBox.setChecked(false);

        initialize();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create, menu);
        MenuItem saveItem = menu.findItem(R.id.menu_create_save);
        if (titleEditText.length() > 0)
            saveItem.setEnabled(true);
    }

    private void initTasks() {
        tasks = new ArrayList<>();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                tasks.addAll(activity.dBhelper.query().getTasks());
                return null;
            }
        }.execute();
    }

    private void saveTask() {
        String title = titleEditText.getText().toString();
        MyTask checkTask = new MyTask();
        checkTask.setTitle(title);
        if (tasks.contains(checkTask)) {
            Toast.makeText(activity, R.string.clone_task_warning, Toast.LENGTH_SHORT).show();
            return;
        }
        myTask.setTitle(title);
        myTask.setDescr(descrEditText.getText().toString());
        if (dateEditText.length() != 0 || timeEditText.length() != 0) {
            myTask.setDate(calendar.getTimeInMillis());
        }
        myTask.setCritical(checkBox.isChecked());
        myTask.setStatus(MyTask.STATUS_CURRENT);
        Log.d("TAG", "saveTask onEdit " + myTask.getTitle());
    }

    private void initialize() {
        titleEditText.setText(myTask.getTitle());
        titleEditText.setSelection(timeEditText.length());
        if (myTask.getDate() != 0)
            dateEditText.setText(Utils.getDate(myTask.getDate()));
        timeEditText.setText(Utils.getTime(myTask.getDate()));
        descrEditText.setText(myTask.getDescr());
        checkBox.setChecked(myTask.isCritical());
    }
}
