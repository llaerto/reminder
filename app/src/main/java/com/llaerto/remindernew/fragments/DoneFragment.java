package com.llaerto.remindernew.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.llaerto.remindernew.R;
import com.llaerto.remindernew.adapters.DoneTaskAdapter;
import com.llaerto.remindernew.database.DBhelper;
import com.llaerto.remindernew.model.MyTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DoneFragment extends TaskFragment {

    OnTaskRestoreListener onTaskRestoreListener;

    public interface OnTaskRestoreListener {
        void onTaskRestore(MyTask myTask);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onTaskRestoreListener = (OnTaskRestoreListener) activity;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_done, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_done);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new DoneTaskAdapter(this);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void addTask(MyTask myTask, boolean saveToDb) {
        Log.d("TAG", "addTask doneFragment " + myTask.getTitle() + " " +saveToDb);
        int position = -1;
        for (int i = 0; i < adapter.getItemCount(); i++) {
            MyTask task = (MyTask) adapter.getItem(i);
            if (myTask.getDate() < task.getDate()) {
                position = i;
                break;
            }
        }

        if (position != -1) {
            adapter.addItem(position, myTask);
        } else {
            adapter.addItem(myTask);
        }

        if (saveToDb) {
            activity.dBhelper.saveTask(myTask);
        }
    }

    @Override
    public void addTaskFromDB() {
        Log.d("TAG", "addTaskFromDB doneFragment");
        adapter.removeAllItems();
        Log.d("TAG", "adapter itemcount = " + adapter.getItemCount());
        List<MyTask> myTasks = new ArrayList<>();
        if (activity.dBhelper == null)
            return;
        myTasks.addAll(activity.dBhelper.query().getTasks(DBhelper.SELECTION_STATUS,
                new String[]{Integer.toString(MyTask.STATUS_DONE)}, DBhelper.TABLE_COLUMN_DATE));
        Log.d("TAG", "myTasks.addAll (addTaskFromDB doneFragment), myTasks.size is " + myTasks.size());
        for (int i = 0; i < myTasks.size(); i++) {
            addTask(myTasks.get(i), false);
        }
    }

    @Override
    public void moveTask(MyTask myTask) {
        Log.d("TAG", "moveTask doneFragment " + myTask.getTitle());
        if (myTask.getDate() != 0 && myTask.getDate() > new Date().getTime()) {
            alarmHelper.setAlarm(myTask);
        }
        onTaskRestoreListener.onTaskRestore(myTask);
    }

    @Override
    public void findTask(String title) {
        Log.d("TAG", "findTask doneFragment " + title);
        checkAdapter();
        Log.d("TAG", "adapter after check size is " + adapter.getItemCount());
        adapter.removeAllItems();
        List<MyTask> myTasks = new ArrayList<>();
        myTasks.addAll(activity.dBhelper.query().getTasks(DBhelper.SELECTION_TITLE + " AND " + DBhelper.SELECTION_STATUS,
                new String[]{"%" + title + "%", Integer.toString(MyTask.STATUS_DONE)}, DBhelper.TABLE_COLUMN_DATE));
        Log.d("TAG", "myTasks.addAll (findTask doneFragment), myTasks.size is " + myTasks.size() + " title was " + title);
        for (int i = 0; i < myTasks.size(); i++) {
            addTask(myTasks.get(i), false);
        }
    }

    @Override
    public void checkAdapter() {
        Log.d("TAG", "checkAdapter, doneFragment");
        if (adapter == null) {
            adapter = new DoneTaskAdapter(this);
            addTaskFromDB();
        }
    }
}
