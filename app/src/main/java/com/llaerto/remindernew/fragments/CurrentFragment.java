package com.llaerto.remindernew.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.llaerto.remindernew.R;
import com.llaerto.remindernew.adapters.CurrentTaskAdapter;
import com.llaerto.remindernew.database.DBhelper;
import com.llaerto.remindernew.model.MySeparator;
import com.llaerto.remindernew.model.MyTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class CurrentFragment extends TaskFragment {
    OnTaskDoneListener onTaskDoneListener;

    public interface OnTaskDoneListener {
        void onTaskDone(MyTask myTask);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onTaskDoneListener = (OnTaskDoneListener) activity;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_current, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_current);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CurrentTaskAdapter(this);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void addTask(MyTask myTask, boolean saveToDb) {
        Log.d("TAG", "addTask " + myTask.toString() + " " + saveToDb + " CurrentFragment");
        int position = -1;

        MySeparator separator = null;
        for (int i = 0; i < adapter.getItemCount(); i++) {
            if (adapter.getItem(i).isTask()) {
                MyTask task = (MyTask) adapter.getItem(i);
                if (myTask.getDate() < task.getDate()) {
                    position = i;
                    Log.d("TAG", "position = " + position);
                    break;
                }
            }
        }

        if (myTask.getDate() != 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(myTask.getDate());

            if (calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
                myTask.setDateStatus(MySeparator.TYPE_OVERDUE);
                Log.d("TAG", "myTask setDateStatus OVERDUE");
                if (!adapter.containsSeparatorOverdue) {
                    Log.d("TAG", "new Separator OVERDUE");
                    adapter.containsSeparatorOverdue = true;
                    separator = new MySeparator(MySeparator.TYPE_OVERDUE);
                }
            } else if (calendar.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
                    && calendar.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
                myTask.setDateStatus(MySeparator.TYPE_TODAY);
                Log.d("TAG", "myTask setDateStatus TODAY");
                if (!adapter.containsSeparatorToday) {
                    Log.d("TAG", "new Separator TODAY");
                    adapter.containsSeparatorToday = true;
                    separator = new MySeparator(MySeparator.TYPE_TODAY);
                }
            } else if (calendar.get(Calendar.DAY_OF_YEAR) == Calendar.getInstance().get(Calendar.DAY_OF_YEAR) + 1
                    && calendar.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
                myTask.setDateStatus(MySeparator.TYPE_TOMORROW);
                Log.d("TAG", "myTask setDateStatus TOMORROW");
                if (!adapter.containsSeparatorTomorrow) {
                    Log.d("TAG", "new Separator TOMORROW");
                    adapter.containsSeparatorTomorrow = true;
                    separator = new MySeparator(MySeparator.TYPE_TOMORROW);
                }
            } else if (calendar.get(Calendar.DAY_OF_YEAR) > Calendar.getInstance().get(Calendar.DAY_OF_YEAR) + 1
                    || calendar.get(Calendar.MONTH) > Calendar.getInstance().get(Calendar.MONTH)
                    || calendar.get(Calendar.YEAR) > Calendar.getInstance().get(Calendar.YEAR)) {
                myTask.setDateStatus(MySeparator.TYPE_FUTURE);
                Log.d("TAG", "myTask setDateStatus FUTURE");
                if (!adapter.containsSeparatorFuture) {
                    Log.d("TAG", "new Separator FUTURE");
                    adapter.containsSeparatorFuture = true;
                    separator = new MySeparator(MySeparator.TYPE_FUTURE);
                }
            }
        }


        if (position != -1) {

            if (!adapter.getItem(position - 1).isTask()) {
                if ((position - 2) >= 0 && adapter.getItem(position - 2).isTask()) {
                    MyTask task = (MyTask) adapter.getItem(position - 2);
                    if (task.getDateStatus() == myTask.getDateStatus()) {
                        position -= 1;
                        Log.d("TAG", "position = " + position);
                    }
                } else if ((position - 2) < 0 && myTask.getDate() == 0) {
                    position -= 1;
                    Log.d("TAG", "position = " + position);
                }
            }

            if (separator != null) {
                adapter.addItem(position - 1, separator);
                Log.d("TAG", "add separator to " + position + " - 1");
            }

            adapter.addItem(position, myTask);
        } else {
            if (separator != null) {
                adapter.addItem(separator);
                Log.d("TAG", "add separator to end");
            }
            adapter.addItem(myTask);
            Log.d("TAG", "additem to adapter");
        }

        if (saveToDb) {
            Log.d("TAG", "saveToDB");
            activity.dBhelper.saveTask(myTask);
        }
    }

    @Override
    public void addTaskFromDB() {
        Log.d("TAG", "addTaskFromDB currentFragment");
        adapter.removeAllItems();
        Log.d("TAG", "adapter itemcount = " +adapter.getItemCount());
        List<MyTask> myTasks = new ArrayList<>();
        if (activity.dBhelper == null)
            return;
        myTasks.addAll(activity.dBhelper.query().getTasks(DBhelper.SELECTION_STATUS + " OR "
                + DBhelper.SELECTION_STATUS, new String[]{Integer.toString(MyTask.STATUS_CURRENT),
                Integer.toString(MyTask.STATUS_OVERDUE)}, DBhelper.TABLE_COLUMN_DATE));
        Log.d("TAG", "myTasks.addAll (addTaskFromDB currentFragment), myTasks.size is " + myTasks.size());
        for (int i = 0; i < myTasks.size(); i++) {
            Log.d("TAG", "loop for addTask from DB, i = " + i);
            addTask(myTasks.get(i), false);
        }
    }

    @Override
    public void moveTask(MyTask myTask) {
        Log.d("TAG", "moveTask currentFragment " + myTask.getTitle());
        alarmHelper.removeAlarm(myTask.getTimeStamp());
        onTaskDoneListener.onTaskDone(myTask);
    }

    @Override
    public void findTask(String title) {
        Log.d("TAG", "findTask " + title);
        checkAdapter();
        Log.d("TAG", "adapter after checkAdapter size is " + adapter.getItemCount());
        adapter.removeAllItems();
        Log.d("TAG", "adapter remove all items");
        List<MyTask> myTasks = new ArrayList<>();
        myTasks.addAll(activity.dBhelper.query().getTasks(DBhelper.SELECTION_TITLE + " AND "
                + DBhelper.SELECTION_STATUS + " OR " + DBhelper.SELECTION_STATUS, new String[]{"%" + title + "%", Integer.toString(MyTask.STATUS_CURRENT),
                Integer.toString(MyTask.STATUS_OVERDUE)}, DBhelper.TABLE_COLUMN_DATE));
        Log.d("TAG", "myTasks.addAll (findTask currentFragment), myTasks.size is " + myTasks.size() + " title was " + title);
        for (int i = 0; i < myTasks.size(); i++) {
            addTask(myTasks.get(i), false);
        }

    }

    @Override
    public void checkAdapter() {
        Log.d("TAG", "checkAdapter, currentFragment");
        if (adapter == null) {
            adapter = new CurrentTaskAdapter(this);
            adapter.removeAllItems();
            addTaskFromDB();
        }
    }
}
