package com.llaerto.remindernew.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.llaerto.remindernew.model.MyTask;

import java.util.Calendar;

/**
 * Created by LaerTo on 29.09.2015.
 */
public class AlarmHelper {
    private static AlarmHelper instance;
    private Context context;
    private AlarmManager alarmManager;

    public static AlarmHelper getInstance() {
        if (instance == null)
            instance = new AlarmHelper();
        return instance;
    }

    public void init(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
    }

    public void setAlarm(MyTask myTask) {
        if (myTask.getDate() != 0 && myTask.getDate() > Calendar.getInstance().getTimeInMillis()) {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("title", myTask.getTitle());
            intent.putExtra("descr", myTask.getDescr());
            intent.putExtra("time_stamp", myTask.getTimeStamp());
            intent.putExtra("color", myTask.getCriticalColor());

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(),
                    (int) myTask.getTimeStamp(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, myTask.getDate(), pendingIntent);
        }
    }

    public void removeAlarm(long timeStamp) {
        Intent intent = new Intent(context, AlarmReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) timeStamp, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.cancel(pendingIntent);
    }

}
