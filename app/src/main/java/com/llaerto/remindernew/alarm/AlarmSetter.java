package com.llaerto.remindernew.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.llaerto.remindernew.database.DBhelper;
import com.llaerto.remindernew.model.MyTask;

import java.util.ArrayList;
import java.util.List;


public class AlarmSetter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DBhelper dBhelper = new DBhelper(context);

        AlarmHelper.getInstance().init(context);
        AlarmHelper alarmHelper = AlarmHelper.getInstance();

        List<MyTask> myTasks = new ArrayList<>();
        myTasks.addAll(dBhelper.query().getTasks(DBhelper.SELECTION_STATUS + " OR "
                + DBhelper.SELECTION_STATUS, new String[]{Integer.toString(MyTask.STATUS_CURRENT),
                Integer.toString(MyTask.STATUS_OVERDUE)}, DBhelper.TABLE_COLUMN_DATE));
        for (MyTask myTask : myTasks) {
            alarmHelper.setAlarm(myTask);

        }
    }
}
