package com.llaerto.remindernew.model;

import com.llaerto.remindernew.R;

/**
 * Created by LaerTo on 29.09.2015.
 */
public class MySeparator implements Item{
    public static final int TYPE_OVERDUE = R.string.overdue;
    public static final int TYPE_TODAY = R.string.today;
    public static final int TYPE_TOMORROW = R.string.tomorrow;
    public static final int TYPE_FUTURE = R.string.future;

    private int type;

    @Override
    public boolean isTask() {
        return false;
    }

    public MySeparator(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
