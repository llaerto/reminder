package com.llaerto.remindernew.model;


import com.llaerto.remindernew.R;

import java.util.Date;

public class MyTask implements Item{
    public static final int STATUS_OVERDUE = 0;
    public static final int STATUS_CURRENT = 1;
    public static final int STATUS_DONE = 2;

    private String title;
    private String descr;
    private long date;
    private boolean critical = false;
    private int status;
    private long timeStamp;
    private int dateStatus;

    @Override
    public boolean isTask() {
        return true;
    }

    public MyTask() {
        this.status = -1;
        this.timeStamp = new Date().getTime();

    }

    public MyTask(String title, String descr, long date, boolean critical, int status, long timeStamp) {
        this.title = title;
        this.descr = descr;
        this.date = date;
        this.critical = critical;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        if (descr != null) {
            return descr;
        }
        else return "";
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isCritical() {
        return critical;
    }

    public void setCritical(boolean isCritical) {
        this.critical = isCritical;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public int getCriticalColor() {
        int i = 1;
        if (!critical)
            i = 2;
        switch (i){
            case 1:
                if (getStatus() == STATUS_CURRENT || getStatus() == STATUS_OVERDUE)
                    return R.color.high_priority;
                else return R.color.high_priority_selected;
            case 2:
                if (getStatus() == STATUS_CURRENT || getStatus() == STATUS_OVERDUE)
                    return R.color.primary;
                else return R.color.accent;
            default:
                return 0;
        }
    }

    public int getCritical() {
        if (this.isCritical())
            return 1;
        else
            return 0;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getDateStatus() {
        return dateStatus;
    }

    public void setDateStatus(int dateStatus) {
        this.dateStatus = dateStatus;
    }
}


