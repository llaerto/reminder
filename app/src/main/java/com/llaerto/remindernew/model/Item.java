package com.llaerto.remindernew.model;

/**
 * Created by LaerTo on 25.09.2015.
 */
public interface Item {

    public boolean isTask();
}
