package com.llaerto.remindernew;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import com.google.android.gms.analytics.Tracker;
import com.llaerto.remindernew.adapters.TabAdapter;
import com.llaerto.remindernew.alarm.AlarmHelper;
import com.llaerto.remindernew.database.DBhelper;
import com.llaerto.remindernew.fragments.CurrentFragment;
import com.llaerto.remindernew.fragments.DoneFragment;
import com.llaerto.remindernew.fragments.SplashFragment;
import com.llaerto.remindernew.fragments.TaskCreateFragment;
import com.llaerto.remindernew.fragments.TaskEditFragment;
import com.llaerto.remindernew.fragments.TaskFragment;
import com.llaerto.remindernew.model.MyTask;

public class MainActivity extends AppCompatActivity
        implements
        TaskCreateFragment.AddingTaskListener,
        CurrentFragment.OnTaskDoneListener,
        DoneFragment.OnTaskRestoreListener,
        TaskEditFragment.TaskEditListener {

    FragmentManager fragmentManager;
    PreferenceHelper preferenceHelper;
    FloatingActionButton fab;
    TabAdapter tabAdapter;
    TaskFragment currentFragment;
    TaskFragment doneFragment;
    SearchView searchView;
    public DBhelper dBhelper;
    private Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getFragmentManager();
        PreferenceHelper.getInstance().init(getApplicationContext());
        preferenceHelper = PreferenceHelper.getInstance();
        dBhelper = new DBhelper(getApplicationContext());
        AlarmHelper.getInstance().init(getApplicationContext());
        //runSplash();

        setUI();

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPause();
    }

    private void runSplash() {
        if (!preferenceHelper.getBoolean(PreferenceHelper.SPLASH_IS_INVISIBLE)) {
            SplashFragment fragment = new SplashFragment();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
        }
    }

    private void setUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icons));
            setSupportActionBar(toolbar);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.current_task));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.done_task));

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabAdapter = new TabAdapter(fragmentManager, 2);
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        currentFragment = (CurrentFragment) tabAdapter.getItem(TabAdapter.CURRENT_TASK_FRAGMENT_POSITION);
        doneFragment = (DoneFragment) tabAdapter.getItem(TabAdapter.DONE_TASK_FRAGMENT_POSITION);

        searchView = (SearchView) findViewById(R.id.search_button);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                currentFragment.findTask(newText);
                doneFragment.findTask(newText);
                return true;
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.main_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskCreateFragment taskCreateFragment = new TaskCreateFragment();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.add(R.id.container, taskCreateFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public void onTaskAdded(MyTask myTask) {
        Log.d("TAG", "onTaskAdded mainActivity");
        currentFragment.addTask(myTask, true);
    }

    @Override
    public void onTaskDone(MyTask myTask) {
        Log.d("TAG", "onTaskDone mainActivity");
        doneFragment.addTask(myTask, false);
    }

    @Override
    public void onTaskRestore(MyTask myTask) {
        Log.d("TAG", "onTaskRestore mainActivity");
        currentFragment.addTask(myTask, false);
    }

    @Override
    public void onTaskEditListener(MyTask updateTask) {
        Log.d("TAG", "onTaskEditListener mainActivity");
        currentFragment.updateTask(updateTask);
        dBhelper.update().task(updateTask);
    }

    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if (count == 0) {
            if (!searchView.isIconified()) {
                searchView.setIconified(true);
            } else {
                super.onBackPressed();
            }
        } else {
            fragmentManager.popBackStack();
        }
    }
}
