package com.llaerto.remindernew.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.llaerto.remindernew.model.MyTask;

/**
 * Created by LaerTo on 28.09.2015.
 */
public class DBUpdateManager {

    private SQLiteDatabase database;
    DBUpdateManager (SQLiteDatabase database) {
        this.database = database;
    }

    public void title(long timeStamp, String title) {
        update(DBhelper.TABLE_COLUMN_TITLE, timeStamp, title);
        Log.d("TAG", "update title " + title);
    }

    public void date(long timeStamp, long date) {
        update(DBhelper.TABLE_COLUMN_DATE, timeStamp, date);
        Log.d("TAG", "update date " + date);
    }

    public void prior(long timeStamp, int prior) {
        update(DBhelper.TABLE_COLUMN_PRIORITY, timeStamp, prior);
        Log.d("TAG", "update priority " + prior);
    }

    public void descr(long timeStamp, String descr) {
        update(DBhelper.TABLE_COLUMN_DESCR, timeStamp, descr);
        Log.d("TAG", "update descr " + descr);
    }

    public void status(long timeStamp, int status) {
        update(DBhelper.TABLE_COLUMN_STATUS, timeStamp, status);
        Log.d("TAG", "update status " + status);
    }

    public void task(MyTask myTask) {
        title(myTask.getTimeStamp(), myTask.getTitle());
        date(myTask.getTimeStamp(), myTask.getDate());
        descr(myTask.getTimeStamp(), myTask.getDescr());
        prior(myTask.getTimeStamp(), myTask.getCritical());
        status(myTask.getTimeStamp(), myTask.getStatus());
        Log.d("TAG", "update TASK");
    }

    private void update(String column, long key, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(column, value);
        database.update(DBhelper.TABLE_NAME, contentValues, DBhelper.TABLE_COLUMN_TIMESTAMP + " = " + key, null);
        Log.d("TAG", "update: " + column + " with key: " + key + " & value: " + value);
    }

    private void update(String column, long key, long value)    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(column, value);
        database.update(DBhelper.TABLE_NAME, contentValues, DBhelper.TABLE_COLUMN_TIMESTAMP + " = " + key, null);
        Log.d("TAG", "update: " + column + " with key: " + key + " & value: " + value);
    }
}
