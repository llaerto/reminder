package com.llaerto.remindernew.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.llaerto.remindernew.model.MyTask;

import java.util.ArrayList;
import java.util.List;

public class DBQueryManager {

    private SQLiteDatabase database;

    DBQueryManager(SQLiteDatabase database) {
        this.database = database;
    }

    public MyTask getTask(long timeStamp) {
        MyTask myTask = null;
        Cursor cursor = database.query(DBhelper.TABLE_NAME, null,
                DBhelper.SELECTION_TIME,
                new String[]{Long.toString(timeStamp)},
                null, null, null);
        Log.d("TAG", "cursor of getTask: " + cursor.toString());
        if (cursor.moveToFirst()) {
            String title = cursor.getString(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_TITLE));
            Log.d("TAG", "title: " + title);
            long date = cursor.getLong(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_DATE));
            Log.d("TAG", "date: " + date);
            String descr = cursor.getString(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_DESCR));
            Log.d("TAG", "descr: " + descr);
            int priority = cursor.getInt(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_PRIORITY));
            Log.d("TAG", "priority:  " + priority);
            int status = cursor.getInt(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_STATUS));
            Log.d("TAG", "status: " + status);
            boolean prior;
            if (priority == 1)
                prior = true;
            else
                prior = false;
            myTask = new MyTask(title, descr, date, prior, status, timeStamp);
            Log.d("TAG", "getTask creating myTask");
        }
        cursor.close();
        return myTask;
    }

    public List<MyTask> getTasks() {
        List<MyTask> tasks = new ArrayList<>();
        Cursor cursor = database.query(DBhelper.TABLE_NAME, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                String title = cursor.getString(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_TITLE));
                Log.d("TAG", "getTasks title: " + title);
                long date = cursor.getLong(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_DATE));
                Log.d("TAG", "getTasks date: " + date);
                String descr = cursor.getString(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_DESCR));
                Log.d("TAG", "getTasks descr: " + descr);
                int priority = cursor.getInt(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_PRIORITY));
                Log.d("TAG", "getTasks priority:  " + priority);
                int status = cursor.getInt(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_STATUS));
                Log.d("TAG", "getTasks status: " + status);
                long timestamp = cursor.getLong(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_TIMESTAMP));
                Log.d("TAG", "getTasks timestamp: " + timestamp);
                boolean prior;
                if (priority == 1)
                    prior = true;
                else
                    prior = false;
                MyTask myTask = new MyTask(title, descr, date, prior, status, timestamp);
                Log.d("TAG", "getTasks creating myTask");
                tasks.add(myTask);
                Log.d("TAG", "tasks size: " + tasks.size());
            } while (cursor.moveToNext());
        }

        cursor.close();
        Log.d("TAG", "tasks size after cursor close: " + tasks.size());
        return tasks;
    }

    public List<MyTask> getTasks(String selection, String[] selectionArgs, String orderBy) {
        List<MyTask> tasks = new ArrayList<>();

        Cursor cursor = database.query(DBhelper.TABLE_NAME, null, selection, selectionArgs, null, null, orderBy);
        Log.d("TAG", "cursor of getTasks: " + cursor.toString());

        if (cursor.moveToFirst()) {
            do {
                String title = cursor.getString(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_TITLE));
                Log.d("TAG", "getTasks title: " + title);
                long date = cursor.getLong(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_DATE));
                Log.d("TAG", "getTasks date: " + date);
                String descr = cursor.getString(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_DESCR));
                Log.d("TAG", "getTasks descr: " + descr);
                int priority = cursor.getInt(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_PRIORITY));
                Log.d("TAG", "getTasks priority:  " + priority);
                int status = cursor.getInt(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_STATUS));
                Log.d("TAG", "getTasks status: " + status);
                long timestamp = cursor.getLong(cursor.getColumnIndex(DBhelper.TABLE_COLUMN_TIMESTAMP));
                Log.d("TAG", "getTasks timestamp: " + timestamp);
                boolean prior;
                if (priority == 1)
                    prior = true;
                else
                    prior = false;
                MyTask myTask = new MyTask(title, descr, date, prior, status, timestamp);
                Log.d("TAG", "getTasks creating myTask");
                tasks.add(myTask);
                Log.d("TAG", "tasks size: " + tasks.size());
            } while (cursor.moveToNext());
        }

        cursor.close();
        Log.d("TAG", "tasks size after cursor close: " + tasks.size());
        return tasks;
    }
}
