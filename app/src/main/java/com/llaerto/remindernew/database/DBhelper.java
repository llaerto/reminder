package com.llaerto.remindernew.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.llaerto.remindernew.model.MyTask;

/**
 * Created by LaerTo on 28.09.2015.
 */
public class DBhelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static final String DATABASE = "reminder_database";
    public static final String TABLE_NAME = "tasks_table";
    public static final String TABLE_COLUMN_TITLE = "task_title";
    public static final String TABLE_COLUMN_DATE = "task_date";
    public static final String TABLE_COLUMN_DESCR = "task_descr";
    public static final String TABLE_COLUMN_PRIORITY = "task_priority";
    public static final String TABLE_COLUMN_STATUS = "task_status";
    public static final String TABLE_COLUMN_TIMESTAMP = "task_timestamp";

    public static final String TABLE_CREATE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TABLE_COLUMN_TITLE + " TEXT NOT NULL, "
            + TABLE_COLUMN_DATE + " LONG, "
            + TABLE_COLUMN_DESCR + " TEXT, "
            + TABLE_COLUMN_PRIORITY + " INTEGER, "
            + TABLE_COLUMN_STATUS + " INTEGER, "
            + TABLE_COLUMN_TIMESTAMP + " LONG);";

    public static final String SELECTION_STATUS = DBhelper.TABLE_COLUMN_STATUS + " = ?";
    public static final String SELECTION_TIME = DBhelper.TABLE_COLUMN_TIMESTAMP + " = ?";
    public static final String  SELECTION_TITLE = DBhelper.TABLE_COLUMN_TITLE + " LIKE ?";

    private DBQueryManager dbQueryManager;
    private DBUpdateManager dbUpdateManager;

    public DBhelper(Context context) {
        super(context, DATABASE, null, DB_VERSION);
        dbQueryManager = new DBQueryManager(getReadableDatabase());
        dbUpdateManager = new DBUpdateManager(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        Log.d("TAG", TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_NAME);
        onCreate(db);
    }

    public void saveTask(MyTask myTask) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_COLUMN_TITLE, myTask.getTitle());
        contentValues.put(TABLE_COLUMN_DATE, myTask.getDate());
        contentValues.put(TABLE_COLUMN_DESCR, myTask.getDescr());
        contentValues.put(TABLE_COLUMN_PRIORITY, myTask.getCritical());
        contentValues.put(TABLE_COLUMN_STATUS, myTask.getStatus());
        contentValues.put(TABLE_COLUMN_TIMESTAMP, myTask.getTimeStamp());
        Log.d("TAG", contentValues.toString() + " saveTask method");
        getWritableDatabase().insert(TABLE_NAME, null, contentValues);
        Log.d("TAG", "inserted to " + TABLE_NAME);
    }

    public DBQueryManager query() {
        return dbQueryManager;
    }

    public DBUpdateManager update() {
        return dbUpdateManager;
    }

    public void removeTask(long timeStamp) {
        getWritableDatabase().delete(TABLE_NAME, SELECTION_TIME, new String[]{Long.toString(timeStamp)});
    }


}
