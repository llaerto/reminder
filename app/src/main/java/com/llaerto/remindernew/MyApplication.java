package com.llaerto.remindernew;

import android.app.Application;

/**
 * Created by LaerTo on 29.09.2015.
 */
public class MyApplication extends Application {
    private static boolean activityVisible;

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResume() {
        activityVisible = true;
    }

    public static void activityPause() {
        activityVisible = false;
    }
}
