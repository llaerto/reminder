package com.llaerto.remindernew.adapters;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.llaerto.remindernew.R;
import com.llaerto.remindernew.Utils;
import com.llaerto.remindernew.fragments.CurrentFragment;
import com.llaerto.remindernew.model.Item;
import com.llaerto.remindernew.model.MySeparator;
import com.llaerto.remindernew.model.MyTask;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class CurrentTaskAdapter extends TaskAdapter {
    private static final int TYPE_TASK = 0;
    private static final int TYPE_SEPARATOR = 1;


    public CurrentTaskAdapter(CurrentFragment taskFragment) {
        super(taskFragment);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case TYPE_TASK:
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.model_task, viewGroup, false);
                TextView titleTextView = (TextView) v.findViewById(R.id.task_title);
                TextView dateTextView = (TextView) v.findViewById(R.id.task_date);
                CircleImageView circleImageView = (CircleImageView) v.findViewById(R.id.circle_image);
                return new TaskViewHolder(v, titleTextView, dateTextView, circleImageView);
            case TYPE_SEPARATOR:
                View separator = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.model_separator, viewGroup, false);
                TextView type = (TextView) separator.findViewById(R.id.tv_separator);
                return new SeparatorViewHolder(separator, type);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        Item item = items.get(position);
        final Resources resources = viewHolder.itemView.getResources();
        if (item.isTask()) {
            viewHolder.itemView.setEnabled(true);
            final MyTask task = (MyTask) item;
            final TaskViewHolder taskViewHolder = (TaskViewHolder) viewHolder;
            final View itemView = taskViewHolder.itemView;

            taskViewHolder.titleTextView.setText(task.getTitle());
            if (task.getDate() != 0) {
                taskViewHolder.dateTextView.setText(Utils.getFullTime(task.getDate()));
            } else {
                taskViewHolder.dateTextView.setText(null);
            }

            itemView.setVisibility(View.VISIBLE);
            taskViewHolder.priority.setEnabled(true);

            if (task.getDate() != 0 && task.getDate() < Calendar.getInstance().getTimeInMillis()) {
                itemView.setBackgroundColor(resources.getColor(R.color.gray_200));
            }

            itemView.setBackgroundColor(resources.getColor(R.color.gray_50));
            taskViewHolder.titleTextView.setTextColor(resources.getColor(R.color.primary_text));
            taskViewHolder.dateTextView.setTextColor(resources.getColor(R.color.secondary_text));
            taskViewHolder.priority.setColorFilter(resources.getColor(task.getCriticalColor()));
            taskViewHolder.priority.setImageResource(R.drawable.ic_checkbox_blank_circle_white_48dp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getTaskFragment().showTaskEditFragment(task);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getTaskFragment().removeTaskDialog(taskViewHolder.getLayoutPosition());
                        }
                    }, 1000);
                    return true;
                }
            });
            taskViewHolder.priority.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    taskViewHolder.priority.setEnabled(false);
                    task.setStatus(MyTask.STATUS_DONE);
                    getTaskFragment().activity.dBhelper.update().status(task.getTimeStamp(), MyTask.STATUS_DONE);
                    itemView.setBackgroundColor(resources.getColor(R.color.gray_50));
                    taskViewHolder.titleTextView.setTextColor(resources.getColor(R.color.primary_text_disabled));
                    taskViewHolder.dateTextView.setTextColor(resources.getColor(R.color.secondary_text_disabled));
                    taskViewHolder.priority.setColorFilter(resources.getColor(task.getCriticalColor()));

                    ObjectAnimator flipIn = ObjectAnimator.ofFloat(taskViewHolder.priority, "rotationY", -180f, 0f);
                    flipIn.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (task.getStatus() == MyTask.STATUS_DONE) {
                                taskViewHolder.priority.setImageResource(R.drawable.ic_check_circle_white_48dp);
                                ObjectAnimator translationX = ObjectAnimator.ofFloat(itemView, "translationX", 0f, itemView.getWidth());

                                ObjectAnimator translationXBack = ObjectAnimator.ofFloat(itemView, "translationX", itemView.getWidth(), 0f);
                                translationX.addListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        itemView.setVisibility(View.GONE);
                                        getTaskFragment().moveTask(task);
                                        removeItem(taskViewHolder.getLayoutPosition());
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                                AnimatorSet translationSet = new AnimatorSet();
                                translationSet.play(translationX).before(translationXBack);
                                translationSet.start();
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    flipIn.start();

                }
            });
        } else {
            MySeparator separator = (MySeparator) item;
            SeparatorViewHolder separatorViewHolder = (SeparatorViewHolder) viewHolder;
            separatorViewHolder.type.setText(resources.getString(separator.getType()));
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (getItem(position).isTask())
            return 0;
        else
            return 1;
    }

}
