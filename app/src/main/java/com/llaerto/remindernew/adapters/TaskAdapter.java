package com.llaerto.remindernew.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.llaerto.remindernew.fragments.TaskFragment;
import com.llaerto.remindernew.model.Item;
import com.llaerto.remindernew.model.MySeparator;
import com.llaerto.remindernew.model.MyTask;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public abstract class TaskAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Item> items;
    TaskFragment taskFragment;
    public boolean containsSeparatorOverdue;
    public boolean containsSeparatorToday;
    public boolean containsSeparatorTomorrow;
    public boolean containsSeparatorFuture;

    public TaskAdapter(TaskFragment taskFragment) {
        this.taskFragment = taskFragment;
        items = new ArrayList<>();
    }

    public Item getItem(int position) {
        return items.get(position);
    }

    public void addItem(Item item) {
        items.add(item);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addItem(int position, Item item) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        if (position >= 0 && position <= getItemCount() - 1) {
            items.remove(position);
            notifyItemRemoved(position);
            if (position - 1 >= 0 && position <= getItemCount() - 1) {
                if (!getItem(position).isTask() && !getItem(position - 1).isTask()) {
                    MySeparator separator = (MySeparator) getItem(position - 1);
                    checkSeparators(separator.getType());
                    items.remove(position - 1);
                    notifyItemRemoved(position - 1);
                }
            } else if (getItemCount() - 1 >= 0 && !getItem(getItemCount() - 1).isTask()) {
                MySeparator separator = (MySeparator) getItem(getItemCount() - 1);
                checkSeparators(separator.getType());

                int locationTemp = getItemCount() - 1;
                items.remove(locationTemp);
                notifyItemRemoved(locationTemp);
            }
        }
    }

    public void checkSeparators(int type) {
        switch (type) {
            case MySeparator.TYPE_OVERDUE:
                containsSeparatorOverdue = false;
                break;
            case MySeparator.TYPE_TODAY:
                containsSeparatorToday = false;
                break;
            case MySeparator.TYPE_TOMORROW:
                containsSeparatorTomorrow = false;
                break;
            case MySeparator.TYPE_FUTURE:
                containsSeparatorFuture = false;
                break;
        }
    }

    public void removeAllItems() {
        if (getItemCount() != 0) {
            items = new ArrayList<>();
            notifyDataSetChanged();
            containsSeparatorFuture = false;
            containsSeparatorTomorrow = false;
            containsSeparatorToday = false;
            containsSeparatorOverdue = false;
        }
    }

    public void updateTask(MyTask myTask) {
        for (int i = 0; i < getItemCount(); i++) {
            if (getItem(i).isTask()) {
                MyTask task = (MyTask) getItem(i);
                if (myTask.getTimeStamp() == task.getTimeStamp()) {
                    removeItem(i);
                    getTaskFragment().addTask(myTask, false);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    protected class SeparatorViewHolder extends RecyclerView.ViewHolder {

        protected TextView type;


        public SeparatorViewHolder(View itemView, TextView type) {
            super(itemView);
            this.type = type;
        }
    }

    protected class TaskViewHolder extends RecyclerView.ViewHolder {
        protected TextView titleTextView;
        protected TextView dateTextView;
        protected CircleImageView priority;

        public TaskViewHolder(View itemView, TextView titleTextView, TextView dateTextView, CircleImageView priority) {
            super(itemView);
            this.titleTextView = titleTextView;
            this.dateTextView = dateTextView;
            this.priority = priority;
        }
    }

    public TaskFragment getTaskFragment() {
        return taskFragment;
    }
}
