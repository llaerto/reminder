package com.llaerto.remindernew.adapters;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.llaerto.remindernew.R;
import com.llaerto.remindernew.Utils;
import com.llaerto.remindernew.fragments.DoneFragment;
import com.llaerto.remindernew.model.Item;
import com.llaerto.remindernew.model.MyTask;

import de.hdodenhof.circleimageview.CircleImageView;


public class DoneTaskAdapter extends TaskAdapter {

    public DoneTaskAdapter(DoneFragment taskFragment) {
        super(taskFragment);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.model_task, viewGroup, false);
        TextView titleTextView = (TextView) v.findViewById(R.id.task_title);
        TextView dateTextView = (TextView) v.findViewById(R.id.task_date);
        CircleImageView circleImageView = (CircleImageView) v.findViewById(R.id.circle_image);

        return new TaskViewHolder(v, titleTextView, dateTextView, circleImageView);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Item item = items.get(position);

        if (item.isTask()) {
            viewHolder.itemView.setEnabled(true);
            final MyTask task = (MyTask) item;
            final TaskViewHolder taskViewHolder = (TaskViewHolder) viewHolder;
            final View itemView = taskViewHolder.itemView;
            final Resources resources = itemView.getResources();

            taskViewHolder.titleTextView.setText(task.getTitle());
            if (task.getDate() != 0) {
                taskViewHolder.dateTextView.setText(Utils.getFullTime(task.getDate()));
            } else {
                taskViewHolder.dateTextView.setText(null);
            }

            itemView.setVisibility(View.VISIBLE);
            taskViewHolder.priority.setEnabled(true);
            taskViewHolder.titleTextView.setTextColor(resources.getColor(R.color.primary_text_disabled));
            taskViewHolder.dateTextView.setTextColor(resources.getColor(R.color.secondary_text_disabled));
            taskViewHolder.priority.setColorFilter(resources.getColor(task.getCriticalColor()));
            taskViewHolder.priority.setImageResource(R.drawable.ic_check_circle_white_48dp);
            taskViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getTaskFragment().removeTaskDialog(taskViewHolder.getLayoutPosition());
                        }
                    }, 1000);
                    return true;
                }
            });

            taskViewHolder.priority.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    taskViewHolder.priority.setEnabled(false);
                    task.setStatus(MyTask.STATUS_CURRENT);
                    getTaskFragment().activity.dBhelper.update().status(task.getTimeStamp(), MyTask.STATUS_CURRENT);
                    taskViewHolder.titleTextView.setTextColor(resources.getColor(R.color.primary_text));
                    taskViewHolder.dateTextView.setTextColor(resources.getColor(R.color.secondary_text));
                    taskViewHolder.priority.setColorFilter(resources.getColor(task.getCriticalColor()));

                    ObjectAnimator flipIn = ObjectAnimator.ofFloat(taskViewHolder.priority, "rotationY", 180f, 0f);
                    taskViewHolder.priority.setImageResource(R.drawable.ic_checkbox_blank_circle_white_48dp);
                    flipIn.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (task.getStatus() != MyTask.STATUS_DONE) {

                                ObjectAnimator translationX = ObjectAnimator.ofFloat(itemView, "translationX", 0f, -itemView.getWidth());

                                ObjectAnimator translationXBack = ObjectAnimator.ofFloat(itemView, "translationX", -itemView.getWidth(), 0f);
                                translationX.addListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        itemView.setVisibility(View.GONE);
                                        getTaskFragment().moveTask(task);
                                        removeItem(taskViewHolder.getLayoutPosition());
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                                AnimatorSet translationSet = new AnimatorSet();
                                translationSet.play(translationX).before(translationXBack);
                                translationSet.start();
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    flipIn.start();

                }
            });
        }
    }
}
