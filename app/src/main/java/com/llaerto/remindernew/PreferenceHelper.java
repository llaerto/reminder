package com.llaerto.remindernew;

import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceHelper {
    public static final String SPLASH_IS_INVISIBLE = "com.llaerto.reminder.PreferenceHelper.SPLASH";
    private static PreferenceHelper instance;
    private SharedPreferences sharedPreferences;

    private PreferenceHelper() {

    }

    public static PreferenceHelper getInstance() {
        if (instance == null) {
            instance = new PreferenceHelper();
        }
        return instance;
    }

    public void init(Context context) {
        sharedPreferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    }

    public void putBoolean(String key, boolean bool) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, bool);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }
}
