package com.llaerto.remindernew;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Utils {
    public static String getDate(long date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy", Locale.ENGLISH);
        return simpleDateFormat.format(date);
    }

    public static String getTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        return simpleDateFormat.format(time);
    }

    public static String getFullTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm", Locale.ENGLISH);
        return simpleDateFormat.format(time);
    }
}
