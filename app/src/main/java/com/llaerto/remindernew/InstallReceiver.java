package com.llaerto.remindernew;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Mykhailo Diachenko
 * on 21.05.2016.
 */
public class InstallReceiver extends BroadcastReceiver {
    private static final String TAG = "InstallReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "intent " + intent);
        Log.d(TAG, "intent data " + intent.getData());
        Log.d(TAG, "intent data string " + intent.getDataString());
        Log.d(TAG, "intent refferer " + intent.getStringExtra("referrer"));
    }
}
